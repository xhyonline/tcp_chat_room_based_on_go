package userProcess

import (
	"../../common/socket"
	"../../common/utils"
	"encoding/json"
	"fmt"
	"net"
)

var isOnlineChannel=make(chan string,1)
func loginSuccessMenu(conn net.Conn,id string)  {

	fmt.Println("登录成功")
	str:=`
=============================================
1)查看在线好友
2)发起聊天
3)退出系统
==============================================
`
	into:=true
	var input int
	for into {
		fmt.Println(str)
		fmt.Scanln(&input)
		switch input {
		case 1:
			//查看在线用户
			lookOnlineUsers(conn)
		case 2:
			//
			selectFriendToChat(conn,id)
		case 3:
			//退出系统
			into=false
		default:
			fmt.Println("输入有误,请重新输入")
		}
	}
}

func selectFriendToChat(conn net.Conn,id string){
	IntoChat:
	bool:=true
	var fid string	//好友id
	chatbox:=new(socket.ChatBox)
	data:=new(socket.Data)
	for bool {
		//展现当前好友ID
		fmt.Println("当前好友在线情况如下:")
		lookOnlineUsers(conn)
		fmt.Println("请输入对方用户的ID,向他发起聊天,按回车发送聊天请求")
		fmt.Print("对方的id:")
		fmt.Scanln(&fid)
		if fid==id {
			fmt.Println("您不能输入自己的ID与自己聊天,请重新输入")
		}else{

			chatbox.From=id
			chatbox.To=fid
			bool=false
		}
	}
	var message ,input string
	var loop =true
	fmt.Println("请输入发送内容,按回车发送消息")
	for loop {
		fmt.Scanln(&message)
		chatbox.Message=chatbox.From+"对"+chatbox.To+"说:"
		chatbox.Message+=message
		chatbox_byte,err:=json.Marshal(chatbox)
		utils.HanddleError(err,"消息盒子序列化失败")
		data.DataType=socket.MessageType
		data.Message=string(chatbox_byte)
		data_byte,err:=json.Marshal(data)
		utils.HanddleError(err,"消息盒子数据序列化失败")
		//发送前来一次探测,对方Id是否在线,或者存在:
		online:=isOnline(conn,fid)
		if online {
			_,err=conn.Write(data_byte)
			if err!=nil {
				fmt.Println("发送失败")
			}else{
				fmt.Println("发送成功")
			}
		}else{
			fmt.Println("系统提示您:对方可能下线了,也可能是您输了一个不存在的账号")
			for loop {
				fmt.Println("输入1按回车,返回聊天大厅:")
				fmt.Println("输入2按回车,退出系统")
				fmt.Scanln(&input)
				switch input {
				case "1":
					goto IntoChat
				case "2":
					fmt.Println("bye bye")
					loop=false
				default:
					fmt.Println("输入错误,请重新输入")
				}
			}
		}

	}

}

//探测对方是否在线

func isOnline(conn net.Conn,id string) bool{
	online:=false
	data:=new(socket.Data)
	data.DataType=socket.IsOnline
	data.Message=id
	data_byte,err:=json.Marshal(data)
	utils.HanddleError(err,"探测对方是否在线,序列化失败")
	_,err=conn.Write(data_byte)
	if err!=nil {
		fmt.Println("探测对方是否在线发送失败")
	}
	var begin=true
	for begin {
		read:=<-isOnlineChannel
		if read=="1" {
			online=true
			break
		}else{
			break
		}
	}

	return online
}


//查看在线用户
func lookOnlineUsers(conn net.Conn) {
	data:=socket.Data{
		DataType: socket.LookOnlineUser,
		Message:  "1",
	}
	data_byte,err:=json.Marshal(data)
	utils.HanddleError(err,"查看在线用户序列化失败")
	_,err=conn.Write(data_byte)
	utils.HanddleError(err,"查看在线用户发送失败")
}

//保持连接,接收用户上下线信息
func keepReceive(conn net.Conn)  {
	var data=new(socket.Data)
	for  {
		_,read_slice,n,err:=socket.Read(conn)
		err=json.Unmarshal(read_slice[:n],data)
		utils.HanddleError(err,"keepReceive出错")
		switch data.DataType {
		case socket.FriendStatus:
			//好友上下线信息从这里通知
			fmt.Println(data.Message)
		case socket.LookOnlineUser:
			//查看所有在线好友列表
			var usersId []string
			err:=json.Unmarshal([]byte(data.Message),&usersId)
			utils.HanddleError(err,"客户端查看好友在线列表序列化失败")
			fmt.Println("在线用户列表")
			for k,v:=range usersId{
				k++
				fmt.Println("序号",k,":",v)
			}

		case socket.MessageType:
			fmt.Println(data.Message)
		case socket.IsOnline:
			isOnlineChannel<-data.Message
			
		}

		
	}
}