package receive

import (
	"../../common/socket"
	"../../common/utils"
	"../conn"
	"../transfer"
	"encoding/json"
	"fmt"
	"net"
	"os"
)

func Receive(accept net.Conn, transferController *transfer.Transfer) {
	for {
		_, read_slice, n,err := socket.Read(accept)
		if err!=nil{

			break
		}
		var data socket.Data
		err = json.Unmarshal(read_slice[:n], &data)
		utils.HanddleError(err, "反序列化Data失败")
		switch data.DataType {
		case socket.LoginType: //登录类型则校验密码账号
			var login = new(socket.UserLogin)
			byte := []byte(data.Message)
			err = json.Unmarshal(byte, login)
			utils.HanddleError(err, "反序列化账号信息失败")
			var redis = new(conn.OptionRedis)
			res := redis.Option("GET", login.Id)
			var code, msg string
			var onlineUser []string
			if len(res.(string)) > 0 {
				if res == login.Pass {
					//登录成功
					code = "200"
					msg = "登录成功"
					//切片增加用户
					transferController.AddUser(login.Id, accept)
					//获取当前在线所有用户
					onlineUser = transferController.GetOnlineUser()
					//通知其他用户,有人上线了
					var notify = new(transfer.Notify)
					notify.NotifyOtherUserOnline(login.Id)
				}
			} else {
				//登录失败
				code = "500"
				msg = "登录失败,请检查您的账号密码"
			}
			status := socket.Status{
				Code:       code,
				Msg:        msg,
				OnlineUser: onlineUser,
			}
			status_byte, err := json.Marshal(status)
			utils.HanddleError(err, "序列化登录状态码失败")
			status_str := string(status_byte)
			data := socket.Data{
				DataType: socket.LoginStatus,
				Message:  status_str,
			}
			data_byte, err := json.Marshal(data)
			utils.HanddleError(err, "序列化登录状态码失败")
			n, err = accept.Write(data_byte)
		case socket.RegisterType: //注册类型
			var register = new(socket.UserRegister)
			msg := string(data.Message)
			msg_byte := []byte(msg)
			err := json.Unmarshal(msg_byte, register)
			utils.HanddleError(err, "注册反序列化失败")
			//检验redis中是否有注册信息
			redis := new(conn.OptionRedis)
			res := redis.Option("EXISTS", register.Id)
			var status = new(socket.Status)
			if res == 1 {
				status.Code = "500"
				status.Msg = "该账号被注册过了"
			} else {
				res := redis.Option("SET", register.Id, register.Pass)
				if res == "OK" {
					status.Code = "200"
					status.Msg = "注册成功"
				} else {
					fmt.Println("注册失败")
					os.Exit(1)
				}
			}
			byte, err := json.Marshal(status)
			utils.HanddleError(err, "注册返回状态失败")
			data := socket.Data{
				DataType: socket.RegisterStatus,
				Message:  string(byte),
			}
			byte, err = json.Marshal(data)
			utils.HanddleError(err, "注册返回数据状态失败")
			n, err = accept.Write(byte)
		case socket.LookOnlineUser:
			//查看用户在线情况
			users := transfer.Users
			var usersId []string
			for id, _ := range users {
				usersId = append(usersId, id)
			}
			byte, err := json.Marshal(&usersId)
			utils.HanddleError(err, "服务端,查看用户在线情况序列化失败1")
			data := new(socket.Data)
			data.Message = string(byte)
			data.DataType = socket.LookOnlineUser
			data_byte, err := json.Marshal(data)
			utils.HanddleError(err, "服务端,查看用户在线情况序列化失败2")
			accept.Write(data_byte)
		case socket.IsOnline:	//'
			fmt.Println("接收到探测消息")
			id:=data.Message
			isOnline:=false
			for uid,_:=range transfer.Users {
				if uid==id {
					isOnline=true
					break
				}
			}
			data:=new(socket.Data)
			data.DataType=socket.IsOnline
			if isOnline {
				data.Message="1"
			}else{
				data.Message="0"
			}
			byte,_:=json.Marshal(data)
			n,err=accept.Write(byte)

			if err!=nil{
				fmt.Println("探测消息返回失败")
			}else{
				fmt.Println("探测信息转发成功")
			}


		case socket.MessageType:
		//普通消息类型
			message:=data.Message
			message_byte:=[]byte(message)
			chatBox:=new(socket.ChatBox)
			err:=json.Unmarshal(message_byte,chatBox)
			utils.HanddleError(err,"messageType反序列化失败")
			data:=new(socket.Data)
			data.DataType=socket.MessageType
			data.Message=chatBox.Message
			data_byte,_:=json.Marshal(data)
			for id,conn:=range transfer.Users  {
				if id==chatBox.To {
					_,err:=conn.Write(data_byte)
					if err!=nil {
						fmt.Println(id,"发送不成功,但是不提示")
					}
					break
				}
			}

		}

	}
}
