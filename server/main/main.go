package main

import (
	"../../common/utils"
	"../receive"
	"../transfer"
	"net"
)



func main() {
	conn,err:=net.Listen("tcp","0.0.0.0:8080")
	utils.HanddleError(err,"监听出错")
	defer conn.Close()
	var transferController=new(transfer.Transfer)
	var notify=new(transfer.Notify)
	//挂起协程发送心跳包
	notify.HeartBeat()
	//通知用户**下线了
	notify.NotifyOtherUserOffline()


	for {

		accept, err := conn.Accept()

		utils.HanddleError(err,"接收数据出错")
		go receive.Receive(accept,transferController)

	}

}



